/*ЗАДАНИЕ:
1.Разработать функции для выполнения арифметических операций по вариантам. V
2.Оформить статическую библиотеку функций и написать программу, ее использующую. V
3.Переоформить библиотеку, как динамическую, но подгружать статически, при компиляции. V
4.Изменить программу для динамической загрузки функций из библиотеки. V

Вариант задания. 
5.Операции возведения в куб и четвертую степень.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

//extern unsigned int degree3();
//extern unsigned int degree4();

int main(int argc, char **argv) {

	void *ext_library;
	unsigned int (*powerfunc)(int x); // Переменная для хранения адреса функции
	ext_library = dlopen("/home/zetter/labeltex/lab5/libs/libdegree.so", RTLD_LAZY);
		if (!ext_library){
		fprintf(stderr,"dlopen() error: %s\n", dlerror());
		return 1;
    	};
	printf("Libraries loaded!\n");
	int x = atoi (argv[1]);
	//unsigned int oper1, oper2;
	//printf("Введите число:\n");
	//scanf("%lf\n", &x);
	/*oper1 = degree3(x);
	oper2 = degree4(x);
	printf("Число в 3 степени = %u\n", oper1);
	printf("Число в 4 степени = %u\n", oper2); */
	powerfunc = dlsym(ext_library, "degree3");
	printf("Число (%d) в 3 степени = %u\n", x, (*powerfunc)(x));
	powerfunc = dlsym(ext_library, "degree4");
	printf("Число (%d) в 4 степени = %u\n", x, (*powerfunc)(x));
	return 0;
	dlclose(ext_library);
}