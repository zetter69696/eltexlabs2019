/*ЗАДАНИЕ:
1. 
Написать программу, работающую с базой данных в виде массива структур и выполняющую 
последовательный ввод данных в массив и последующую распечатку его содержимого. 
Состав структуры приведен в варианте.
Типы данных выбрать самостоятельно.
При написании программы следует использовать статические массивы структур или 
указателей на структуру.
Размерности массивов – 3–4. 
Ввод данных выполнить с помощью функций scanf().

2. 
Модифицировать программу, используя массив указателей на структуру и динамическое 
выделение памяти. 
Выполнить сортировку массива с помощью функции qsort. 
Способ сортировки массива приведен в варианте.
Для динамического выделения памяти используйте функцию malloc().
Для определения размера структуры в байтах удобно использовать операцию sizeof(), 
возвращающую целую константу: 
	struct ELEM *sp; 
	sp = malloc(sizeof(struct ELEM));

5
Название покупки 
Дата приобретения 
Стоимость 
Количество 

Сгруппировать все записи по месяцам приобретения 
*/

#include <stdio.h>
#include <stdlib.h>
#define YEAR0 2018

struct date{
	unsigned short day   :5;//0-31
	unsigned short month :4;//0-15
	unsigned short year  :7;//0-127
};
typedef struct date today;

struct purchase{
    char name[50];
    int quantity;
    int price;
};
typedef struct purchase purchases;
 

void readPurchase(purchases *st){
    printf("Введите название товара:");
    scanf("%s", st->name);
    printf("Сколько приобретено данного товара:");
    scanf("%d", &st->quantity);
    printf("Введите стоимость за единицу товара:");
    scanf("%d", &st->price);
}

static int cmp(const void *p1, const void *p2){
    purchases * st1 = *(purchases**)p1;
    purchases * st2 = *(purchases**)p2;
    return st2->price - st1->price;
}


int main(int argc, char **argv){
    int count = 3;
    printf("Введите количество совершенных покупок:");
    scanf("%d", &count);
    
    purchases** st = (purchases**)malloc(sizeof(purchases*)*count);
    for (int i = 0; i < count ; i++){
        st[i] = (purchases*) malloc (sizeof(purchases));
        readPurchase(st[i]);
    }

    struct date today;
  	today.day = 3;
  	today.month = 4;
  	today.year = 2019 - YEAR0;
    
    

    qsort(st, count, sizeof(purchases*), cmp);
    printf("Дата приобретения покупки: %u.%u.%u \n", today.day, today.month, today.year+YEAR0);
     for (int i = 0; i < count ; i++){
            printf("Название покупки:%s ", st[i]->name);
            printf("Количество:%d ", st[i]->quantity);
            printf("Цена за ед. товара:%d\n", st[i]->price);
        }

    for (int i = 0; i < count; i++)
    {
        free(st[i]);
    }
    free(st);
    return 0;
}
