/*ЗАДАНИЕ:
В лабораторной работе требуется написать две программы для обработки текстовых файлов. Одна из них 
выполняет построчную, другая посимвольную обработку:
	1. Написать программу, обрабатывающую текстовый файл и записывающую обработанные данные в файл с
	 таким же именем, но с другим типом (табл. 3).
	2. Написать программу, выполняющую посимвольную обработку текстового файла (табл. 4).

Ввод параметров должен быть организован в командной строке запуска программы.
Исходный файл должен быть создан с помощью любого текстового редактора.
При обработке текста рекомендуется использовать функции из стандартной библиотеки СИ для работы со 
строками, преобразования и анализа символов.

Таблица 3.
5
Исключить строки с количеством пробелов, большим заданного числа 
Параметры командной строки:
	1. Имя входного файла 
	2. Заданное количество пробелов 

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 1024
int main (int argc, char **argv)
{
printf("argc = %d\n", argc);
    if (argc < 4){
        printf("Неверное кол-во параметров\n");
        exit(1);
    }
    FILE *fp, *fp2;
    char ch;
    if ((fp = fopen (argv[1], "r")) == NULL) {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }
    if ((fp2 = fopen (argv[2], "w")) == NULL) {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }
    char buffer[N];
    int kolSpace = 0;
    int space = atoi (argv[3]);
    int i = 0;
    while ((ch = fgetc (fp)) != EOF) {
        buffer[i++] = ch;
        printf ("%c", ch);
        if (ch == ' ' ) {
            kolSpace++;
            //printf("Пробел найден!%d", kolSpace);  
        }
        if (ch == '\n') {
        		if (kolSpace < space) {
                for (int j = 0; j < strlen(buffer); j++) {
                    fputc (buffer[j], fp2);
                } 
            }
            memset(buffer, '\0', N);
            kolSpace = 0;
            i = 0;
        	}
    }
    fclose (fp2);
    fclose (fp);
    return 0;
}
