#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 1024
int main (int argc, char **argv)
{
    if (argc < 4) {
        printf("Неверное кол-во параметров\n");
        exit(1);
    }
    FILE *fp, *fp2;
    if ((fp = fopen (argv[1], "r")) == NULL)
    {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }
    if ((fp2 = fopen (argv[2], "w")) == NULL)
    {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }
    char buffer[N];
    int space = atoi (argv[3]);
    int kolSpace = 0;

    while(fgets(buffer, N, fp) != 0) {
        //memset(buffer, '\0', N);
        for(int i = 0; buffer[i] != '\0'; i++) {

            if(buffer[i] == ' ') {
                kolSpace++;
                printf("Пробел найден! %d\n", kolSpace);
            }
        }
        if (kolSpace < space) {
            fputs(buffer, fp2);
            printf("%s", buffer);
        }
        kolSpace = 0;
    }
    fclose (fp2);
    fclose (fp);
    return 0;
}
