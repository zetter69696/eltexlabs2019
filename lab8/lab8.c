/*
ЛАБОРАТОРНАЯ РАБОТА №8
МЕЖПРОЦЕССНЫЕ КОММУНИКАЦИИ В LINUX. ОЧЕРЕДИ СООБЩЕНИЙ.

3. МЕТОДИЧЕСКИЕ УКАЗАНИЯ. 
3.1. Для обмена данными между процессами использовать исключительно очереди сообщений System V.
3.2. Для ознакомления с функциями работы с каналами и очередями сообщений используйте инструкции 
man msgget, man msgsnd, man msgrcv, man msgctl.
3.3. Для отладки рекомендуется использовать отладчик gdb, информацию котором можно получить инструкцией 
man gdb.
3.4. Для отладки и запуска программ, протоколирования их результатов и сохранения на локальном 
компьютере см. методические указания к лабораторной работе №6.

4. ПОРЯДОК ВЫПОЛНЕНИЯ РАБОТЫ. 
4.1. Модифицировать и отладить в соответствии с вариантом и выбранным средством коммуникации 
программу из лабораторной работы №6, реализующую порожденный процесс. При необходимости 
модифицировать ее в отдельную программу-сервер.
4.2. Модифицировать и отладить в соответствии с вариантом и выбранным средством коммуникации 
программу из лабораторной работы №6, реализующую родительский процесс. При необходимости 
модифицировать ее в отдельную программу-клиент.

Вариант 5
Винни-Пух и пчелы. Заданное количество пчел добывают мед равными порциями, задерживаясь в пути 
на случайное время. 
Винни-Пух потребляет мед порциями заданной величины за заданное время и столько же времени 
может прожить без питания. 
Работа каждой пчелы реализуется в порожденном процессе.
*/
// Родительмкий процесс - бочка меда. Дочерние - n-ное кол-во процессов-пчел и один процесс-медведь.
// Каждый процесс - пчела будет производить по одной 1 меда. 
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>



struct mymsgbuf {
	long mtype;		/* тип сообщения, должен быть > О */
	int len;
};
int msgqid, rc;

#define MAX_SEND_SIZE sizeof(struct mymsgbuf)-sizeof(long)
int flag = 1;
void handler(int fflag) {
	flag = 0;
}
// Добавление сообщения в очередь
void send_message(int qid, struct mymsgbuf *qbuf, long type, int len)
{
	qbuf->mtype = type;
	qbuf->len = len;

	if ((msgsnd(qid, (struct msgbuf *)qbuf, MAX_SEND_SIZE, 0)) == -1) 
	{
		perror("msgsnd");
		exit(1);
	}
}
// Чтение сообщения из очереди
void read_message(int qid, struct mymsgbuf *qbuf, long type)
{
	qbuf->mtype = type;
	msgrcv(qid, (struct msgbuf *)qbuf, MAX_SEND_SIZE, type, 0);
	printf("Type: %ld Text: %d\n", qbuf->mtype, qbuf->len);
	fflush(stdout);
}

int main(int argc, char *argv[])
{
	int i, pid[argc], status, stat;
	key_t key;
	int qtype = 1;
	struct mymsgbuf qbuf;
	// Проверка на наличие аргументов
	if (argc < 7) {
		printf("Введите следующие параметры:\n ./lab8 'Вместимость бочки' 'Начальное кол-во меда в бочке' 'Кол-во пчел' 'Кол-во меда, приносимое 1 пчелой' 'Порция' 'Время потребления'\n");
		exit(-1); // 100 20 4 1 5 3
	}

	int bochka = atoi(argv[1]);
	int bochkaStart = atoi(argv[2]);
	int bees = atoi(argv[3]);
	int beeCol = atoi(argv[4]);
	int bearCol = -atoi(argv[5]);
	int bearTime = atoi(argv[6]);

	printf("Вместимость бочки: %d\n", bochka);
	printf("Начальное кол-во меда в бочке: %d\n", bochkaStart);
	printf("Кол-во пчел: %d\n", bees);
	printf("Кол-во меда, приносимое 1 пчелой: %d\n", beeCol);
	printf("Порция: %d\n", bearCol);
	printf("Время потребления: %d\n", bearTime);
	

	// Создание новой очереди сообщений или получение доступа к существующей очереди сообщений
	key = ftok(".", 'm'); /*Преобразует имя файла и идентификатор в ключ для системных вызовов(IPC key)*/
	//printf("key = %d\n", key);
	if ((msgqid = msgget(key, IPC_CREAT | 0660)) == -1) { /* получает идентификатор очереди сообщений */
		perror("msgget");
		exit(1);
	}
	// Пчелы и медведь
	for (i = 0; i <= bees; i++) {
		// запускаем дочерний процесс 
		pid[i] = fork();
		srand(getpid());

		if (-1 == pid[i]) {
			perror("fork");	/* произошла ошибка */
			exit(1);	/*выход из родительского процесса */
		} else if (0 == pid[i]) {
			signal(SIGTERM, handler);
			if(i == 0) {
			while(flag == 1) {
					printf(" CHILD: Медведь проснулся!\n");
			sleep(rand() % 4);
			send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, bearCol);
			printf(" CHILD: Медведь съел %d меда!\n", bearCol);
			}
			exit(0);
		
			} else {
				while(flag == 1) {
			printf(" CHILD: Пчела %d полетела за медом!\n", i);
			sleep(rand() % 4);
			printf(" CHILD: Пчела %d принесла %d меда!\n", i, beeCol);

			//char str[10];
			//sprintf(str, "%d", (int)strlen(argv[i]));
			send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, beeCol);
			printf(" CHILD: Это %d процесс-потомок отправил сообщение!\n", i);
			fflush(stdout);
			}
			exit(0);	
			}
		}
	}
	// если выполняется родительский процесс
	printf("PARENT: Это процесс-родитель!\n");
	// ожидание окончания выполнения всех запущенных процессов
		printf("количество меда в бочке %d\n", bochkaStart);
		fflush(stdout);

	while(bochkaStart > 0) {
	read_message(msgqid, &qbuf, qtype);
	bochkaStart =+ qbuf.len;
	printf("количество меда в бочке %d\n", bochkaStart);
	fflush(stdout);
}

	for (i = 0; i <= bees; i++) {
		kill(pid[i], SIGTERM);
		printf("Процесс %d был убит!\n", pid[i]); 
		fflush(stdout);
	}

    for (i = 0; i < bees; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
            fflush(stdout);
        }
    }

	if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0) { /* выполняет контрольные операции над сообщениями */
		perror(strerror(errno));
		printf("msgctl (return queue) failed, rc = %d\n", rc);
		return 1;
	}
	return 0;
}
