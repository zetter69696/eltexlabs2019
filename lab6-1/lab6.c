/*
4. ПОРЯДОК ВЫПОЛНЕНИЯ РАБОТЫ.
4.1. Ознакомиться с опциями компилятора gcc, методикой отладки программ.

4.2. Для вариантов заданий написать и отладить программу, реализующую порожденный процесс.
4.3. Для вариантов заданий написать и отладить программу, реализующую родительский процесс, вызывающий
 и отслеживающий состояние порожденных процессов - программ (ждущий их завершения или уничтожающий их, в
  зависимости от варианта).
4.4. Для вариантов заданий написать и отладить программу, реализующую родительский процесс, вызывающий и
отслеживающий состояние порожденных процессов - функций (ждущий их завершения или уничтожающий их, в
	зависимости от варианта).

?===============?

1. Поиск указанной строки в указанном файле. Обработка одной строки в порожденном процессе.

?================?
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
  /* reverse:  переворачиваем строку s на месте */
 void reverse(char s[])
 {
     int i, j;
     char c;
 
     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }
 /* itoa:  конвертируем n в символы в s */
 void itoa(int n, char s[])
 {
     int i, sign;
 
     if ((sign = n) < 0)  /* записываем знак */
         n = -n;          /* делаем n положительным числом */
     i = 0;
     do {       /* генерируем цифры в обратном порядке */
         s[i++] = n % 10 + '0';   /* берем следующую цифру */
     } while ((n /= 10) > 0);     /* удаляем */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }

int main(int argc, char **argv) {
		int NumOfStr, t = rand()%4, i = 0, pid, status, stat;
		int Lines_Count = 0;
    	FILE *f_input, *f_output;
    	char ch;
// Проверка наличия аргументов
    	printf("argc = %d\n", argc);
    	printf("argv[0] = %s\n", argv[0]);
        printf("argv[1] = %s\n", argv[1]);
        printf("argv[2] = %s\n", argv[2]);
		if (argc < 3) {
        printf("Используйте: ./lab6 textfile1 textfile2 ...\n");
        exit(-1);
    }
        if ((f_input = fopen (argv[1], "r")) == NULL) {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }
// ПОИСК СТРОКИ В ФАЙЛЕ
// Подсчет количества строк в файле   
    while ((ch = fgetc(f_input)) != EOF)
    {
        if(ch == '\n') {
            Lines_Count++;
        }
    }
    fclose(f_input);
    printf("В файле %d строк(и).\n", Lines_Count);
    printf("Введите номер строки для обработки файла от 0 до %d:\n", --Lines_Count);
	scanf("%d", &NumOfStr);
	while(NumOfStr > Lines_Count || NumOfStr < 0) {
		printf("Вы ввели неверное значение. Попробуйте снова:\n");
		scanf("%d", &NumOfStr);
	}
	itoa(NumOfStr, argv[3]);
	printf("Значение NumOfStr = %s\n", argv[3]);
	printf("Cтрокa для обработки файла: %d\n", NumOfStr);
	// ВЫВОД И ОБРАБОТКА СТРОКИ С ЗАДАННЫМ НОМЕРОМ В ФАЙЛ !!! FOR EXECL()
	    if ((f_input = fopen (argv[1], "r")) == NULL) {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }
    	if ((f_output = fopen (argv[2], "w")) == NULL) {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }
	while ((ch = fgetc(f_input)) != EOF)
    {
        if(i == NumOfStr && ch != '\n') {
		printf ("%c", ch);
		fputc(ch, f_output);
		}
		else if(ch == '\n') {
			i++;
		}
    }
    printf ("\n");
	fclose(f_input);
	fclose(f_output);
// В ПОРОЖДЕННОМ ПРОЦЕССЕ БУДЕТ ПРОИЗВОДИТЬСЯ ВЫВОД СТРОКИ В ФАЙЛ   
        pid = fork();
        if (pid == 0) {
// если выполняется дочерний процесс, запускаем программу child
        	if (execl("./child", "child", argv[1], argv[2], argv[3], NULL) < 0) {
             printf("Ошибка при попытке запуска файла %s или %s\n", argv[1], argv[2]);
             exit(-2);
             }
             else printf("Файлы %s и %s запущены (pid = %d)\n", argv[1], argv[2], pid);
        	}
// если выполняется родительский процесс
    sleep(t);
// ожидание окончания выполнения всех запущенных процессов
        status = waitpid(pid, &stat, WNOHANG);
        if (pid == status) {
            printf("Файлы %s и %s выполнены,  результат = %d\n", argv[1], argv[2], WEXITSTATUS(stat));
        }
    return 0;
}