#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {
int total= 0, i = 0;
FILE *f_input, *f_output;
char ch;

		printf("argc = %d\n", argc);
    	printf("argv[0] = %s\n", argv[0]);
        printf("argv[1] = %s\n", argv[1]);
        printf("argv[2] = %s\n", argv[2]);
        printf("argv[3] = %s\n", argv[3]);

if (argc<3) {
    printf("Usage: file textfile1 textfile2\n");
    exit(-1);
    }

	// ВЫВОД И ОБРАБОТКА СТРОКИ С ЗАДАННЫМ НОМЕРОМ В ФАЙЛ !!! FOR EXECL()

	    if ((f_input = fopen (argv[1], "r")) == NULL) {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }

    	if ((f_output = fopen (argv[2], "w")) == NULL) {
        printf ("Невозможно открыть файл.\n");
        exit (1);
    }

	while ((ch = fgetc(f_input)) != EOF)
    {
        if(i == atoi(argv[3]) && ch != '\n') {
		printf ("%c", ch);
		fputc(ch, f_output);
		total++;
		}
		else if(ch == '\n') {
			i++;
		}
    }
    printf ("\n");
	fclose(f_input);
	fclose(f_output);
 
printf("(PIO: %d), Файл %s, записано символов = %d\n", getpid(), argv[2], total);
return(total); 
}
