/*
ЛАБОРАТОРНАЯ РАБОТА №9
МЕЖПРОЦЕССНЫЕ КОММУНИКАЦИИ В LINUX. СЕМАФОРЫ И РАЗДЕЛЯЕМАЯ ПАМЯТЬ.

3. МЕТОДИЧЕСКИЕ УКАЗАНИЯ. 
3.1. Для обмена данными между процессами рекомендуется использовать разделяемую память. 
Для блокировки одновременного доступа процессов к разделяемой памяти рекомендуется использовать семафоры 
System V.
3.2. Для ознакомления с функциями работы с семафорами и разделяемой памятью используйте инструкции 
man semget, man semop, man semctl и man shmget, man shmat, man shmdt, man msgctl.
3.3. Для отладки рекомендуется использовать отладчик gdb, информацию котором можно получить инструкцией 
man gdb..
3.4. Для отладки и запуска программ, протоколирования их результатов и сохранения на локальном компьютере 
см. методические указания к лабораторной работе №6.

4. ПОРЯДОК ВЫПОЛНЕНИЯ РАБОТЫ. 
4.1. Модифицировать и отладить в соответствии с вариантом и выбранным средством коммуникации программу 
из лабораторной работы №6, реализующую порожденный процесс. При необходимости модифицировать ее в 
отдельную программу-сервер.
4.2. Модифицировать и отладить в соответствии с вариантом и выбранным средством коммуникации программу 
из лабораторной работы №6, реализующую родительский процесс. При необходимости модифицировать ее в 
отдельную программу-клиент.

5. ВАРИАНТ.
4. Warcraft. 
Заданное количество шахтеров добывают золото равными порциями из одной шахты, 
задерживаясь в пути на случайное время, до ее истощения. 
Работа каждого шахтера реализуется в порожденном процессе.

Шахта - разделяемая память?
Обязательно ли давать РП значение double?
*/

//--- sum_shm.c
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>

//#define MAX_LEN 1024
union semun {
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
                           /* часть, особенная для Linux: */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
};

int fflag = 1;
void end(int flag, int *mineSize) {
	if(*mineSize <= 0) {
		fflag = 0;
	}
	printf("В шахте осталось %d золота!\n", *mineSize);
	fflush(stdout);
}

void workOfMiners(int *mineSize, int numOfMiners, int colGold){
	
	pid_t pid[numOfMiners];
	pid_t wpid;
	int status = 0;
	
	int shmid;
	key_t key = 69;
	int *shm;
	
	int semid;
	union semun arg;
	struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
	struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса
	
	 /* Получим ключ, Один и тот же ключ можно использовать как
    для семафора, так и для разделяемой памяти */
	if ((key = ftok(".", 'S')) < 0) {
		printf("   [Невозможно получить ключ]\n");
		exit(1);
	}
	
	/* Создадим семафор - для синхронизации работы с разделяемой памятью.*/
	semid = semget(key, 1, 0666 | IPC_CREAT);
	
	/* Установить в семафоре № 0 (Контроллер ресурса) значение "1" */
	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);
	 
	 /* Создадим область разделяемой памяти */
	if ((shmid = shmget(key, sizeof(double), IPC_CREAT | 0666)) < 0) {
		perror("   [shmget]");
		exit(1);
	}
			int i;

				/* Получим доступ к разделяемой памяти */
				if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
					perror("   [shmat]");
					exit(1);
				}

				*(shm) = *mineSize;

				/* Отключимся от разделяемой памяти */
				if (shmdt(shm) < 0) {
					printf("   [Ошибка отключения]\n");
					exit(1);
				}

	for (i = 1; i <= numOfMiners; i++){

			pid[i] = fork();
			srand(getpid());

			if (0 == pid[i]) {
				while(fflag == 1){

				printf("   [PID = %d i = %d]\n", getpid(), i);
				printf("Шахтер %d пошел в шахту за золотом!\n", i);
				fflush(stdout);
				sleep(rand() % 4);
			
				/* Получим доступ к разделяемой памяти */
				if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
					perror("   [shmat]");
					exit(1);
				}
				
				printf("   [Процесс ожидает PID = %d i = %d]\n", getpid(), i);
				fflush(stdout);
				
				/* Заблокируем разделяемую память */	
				if((semop(semid, &lock_res, 1)) == -1){
					fprintf(stderr, "   [Не удалось заблокировать]\n");
					exit(1);
				} else{
					printf("   [Значение семафора уменьшено на 1 (заблокировано) i = %d]\n", i);
					fflush(stdout);
				}
				end(fflag, shm);
				/* Запишем в разделяемую память количество золота, оставшееся в шахте */
					if(fflag!=0)
				*(shm) = *(shm) - colGold;
				/* Освободим разделяемую память */
				if((semop(semid, &rel_res, 1)) == -1){
					 fprintf(stderr, "   [Не удалось разблокировать]\n");
					 exit(1);
				} else{
					printf("   [Значение семафора увеличено на 1 (разблокировано) i = %d]\n", i);
					fflush(stdout);
				}
				
				 /* Отключимся от разделяемой памяти */
				if (shmdt(shm) < 0) {
					printf("   [Ошибка отключения]\n");
					exit(1);
				}
				printf("Шахтер %d вернулся из шахты!\n", i);
				
				}
				exit(0);
			}	else if (pid[i] < 0){
					perror("   [fork]"); /* произошла ошибка */
					exit(1); /*выход из родительского процесса*/
			}
	}
	
	for (int i = 1; i <= numOfMiners; i++) {
		wpid = waitpid(pid[i], &status, 0);
		if (pid[i] == wpid) {
			printf("   [процесс-потомок %d done,  result = %d]\n", i, WEXITSTATUS(status));
			fflush(stdout);
		}
	}	

	/* Получим доступ к разделяемой памяти */
	if ((shm = (int*)shmat(shmid, NULL, 0)) == (int *) -1) {
		perror("   [shmat]");
		exit(1);
	}

	printf("------------------\n");
	printf("Из шахты было добыто всё золото! %d\n", *(shm));
	fflush(stdout);

	if (shmdt(shm) < 0) {
		printf("   [Ошибка отключения]\n");
		exit(1);
	} 
	
	/* Удалим созданные объекты IPC */	
	 if (shmctl(shmid, IPC_RMID, 0) < 0) {
		printf("   [Невозможно удалить область]\n");
		exit(1);
	} else
		printf("   [Сегмент памяти помечен для удаления]\n");
	
	if (semctl(semid, 0, IPC_RMID) < 0) {
		printf("   [Невозможно удалить семафор]\n");
		exit(1);
	}
}	

int main(int argc, char **argv){

	 if (argc < 4) {
        printf("Используйте: ./lab9.c 'Объем шахты' 'Кол-во шахтеров' 'Кол-во золота, добываемого 1 шахтером'\n");
        exit(-1);
    }

	int mineSize = atoi(argv[1]);
    int numOfMiners = atoi(argv[2]);
    int colGold = atoi(argv[3]); 

    printf("Объем шахты: %d\n", mineSize);
    printf("Кол-во шахтеров: %d\n", numOfMiners);
    printf("Кол-во золота, приносимое одним шахтером: %d\n", colGold);
	workOfMiners(&mineSize, numOfMiners, colGold);
	return 0;
}