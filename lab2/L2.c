/*ЗАДАНИЕ:
Написать программу сортировки массива строк по вариантам.
Строки вводить с клавиатуры. Сначала пользователь вводит кол-во строк потом сами строки. 
Для тестирования использовать перенаправление ввода-вывода ($./myprog < test.txt). 
Память для строк выделять динамически с помощью функций malloc или calloc.
Ввод данных, сортировку и вывод результатов оформить в виде функций.
Сортировку делать с помощью qsort если возможно, если нет то писать свой вариант сортировки.
Входные и выходные параметры функции сортировки указаны в варианте. 
Входные и выходные параметры функций для ввода-вывода:

Прототип функции для ввода одной строки 
length = inp_str(char* string, int maxlen); 
// length – длина строки 
// string – введенная строка 
// maxlen – максимально возможная длина строки (размерность массива string) 

Прототип функции для вывода одной строки.
void out_str(char* string, int length, int number);
// string – выводимая строка 
// length – длина строки 
// number – номер строки 

Модифицировать программу, применив в функциях передачу параметров и возврат результатов по ссылке (с использованием указателей).
Сравнить результаты.

5
Расположить строки по возрастанию количества слов 
Входные параметры:
1. Массив 
2. Размерность массива 
Выходные параметры:
1. Количество перестановок 
2. Первый символ последней строки 
*/

#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h> 
#include <string.h>
#define N 1024

char** readMas(int count){
	char buffer[N];
	char **mas;  //указатель на массив указателей на строки
	mas = (char **)malloc(sizeof(char *)*count);// выделяем память для массива указателей
    for (int i = 0; i < count ; i++){
        scanf("%[^\n]%*c", buffer); // читаем строку в буфер
        mas[i] = (char *)malloc(sizeof(char)*strlen(buffer)); //выделяем память для строки
        strcpy(mas[i], buffer); //копируем строку из буфера в массив указателей
    }
    return mas; 
}

void printMas(char **mas, int count){
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
    }
}

int PrintColMas(int *mas, int n)
{

    printf("\nКоличество слов в строках:\n");
    for (int i = 0; i < n; i++) {
        printf("%d\n", mas[i]);
    }

}

int Sort(char **A, int n, int m)
{
    int ColMas[n];
    int k, s = 0;
    for (int i = 0; i < n; i++)
    {
    	k = 0;
    	printf("\nCтрока %s", A[i]);
        for (int j = 0; j < strlen(A[i]); j++)
        {
            switch (A[i][j])
            {
            case ' ':
                k++;
                break;
            //default:
              //  printf("Пробелов в строке нет!\n");
                //break;
            }

        }
        printf("\nCтрока %d Пробелов %d", i, k);
        ColMas[i] = k;
        
    }

    PrintColMas(ColMas, n);

    for (int i = 0; i < n - 1; i++)
    {
        for (int f = 0; f < n - i - 1; f++)
        {
            if (ColMas[f] < ColMas[f+1])
            {

                k = ColMas[f];
                ColMas[f] = ColMas[f+1];
                ColMas[f+1] = k;
                char *buf;
                buf = A[f];
                A[f] = A[f+1];
                A[f+1] = buf;

             }

        }
    }

    PrintColMas(ColMas, n);

}

int main() {
    int n, m;
    printf("Введите число строк:");
    scanf("%d", &n);
    printf("Введите длину строки:");
    scanf("%d\n", &m);
    char **B;
    
    B = readMas(n);
    printMas(B, n);
    Sort(B, n, m);
	printMas(B, n);
    
    for (int i = 0; i < n; i++) {
        free(B[i]);
    }
    free(B);

}