/*ЗАДАНИЕ:
Написать программу сортировки массива строк по вариантам.
Строки вводить с клавиатуры. Сначала пользователь вводит кол-во строк потом сами строки. 
Для тестирования использовать перенаправление ввода-вывода ($./myprog < test.txt). 
Память для строк выделять динамически с помощью функций malloc или calloc.
Ввод данных, сортировку и вывод результатов оформить в виде функций.
Сортировку делать с помощью qsort если возможно, если нет то писать свой вариант сортировки.
Входные и выходные параметры функции сортировки указаны в варианте. 
Входные и выходные параметры функций для ввода-вывода:

Прототип функции для ввода одной строки 
length = inp_str(char* string, int maxlen); 
// length – длина строки 
// string – введенная строка 
// maxlen – максимально возможная длина строки (размерность массива string) 

Прототип функции для вывода одной строки.
void out_str(char* string, int length, int number);
// string – выводимая строка 
// length – длина строки 
// number – номер строки 

Модифицировать программу, применив в функциях передачу параметров и возврат результатов по ссылке (с использованием указателей).
Сравнить результаты.*/


//В5
//Расположить строки по возрастанию количества слов 
//Входные параметры:
//1. Массив 
//2. Размерность массива 
//Выходные параметры:
//1. Количество перестановок 
//2. Первый символ последней строки 

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#define MAX_LEN 1024


char** readMas(int N, int M){
	char buffer[MAX_LEN];
	char **mas;  //указатель на массив указателей на строки
	mas = (char **)malloc(sizeof(char *)*N*M);// выделяем память для массива указателей
    for (int i = 0; i < N; i++){
    	for (int j = 0; j < M; j++) {
    	scanf("%s", buffer); // читаем строку в буфер
        mas[i][j] = (char *)malloc(sizeof(char)*strlen(buffer)); //выделяем память для строки
        strcpy(mas[i][j], buffer); //копируем строку из буфера в массив указателей	
    	}
  
    }
    return mas; 
}


/*
 void Input(){
for (int i; i < N; i++){
	for(int j; j < M; j++){
		//length = inp_str(char* string, int maxlen);
		scanf("%s", A[i][j]);
	}
	}
}

void Sort(){
for (int i = 0; i < N; i++){
		strlen(A[][]);
	}

}
*/
void printMas(char *mas, int count){
	for (int i; i < N; i++){
		for(int j; j < M; j++){
			//void out_str(char* string, int length, int number);
			printf("%s", A[i][j]);
	}
	printf("\n");
	}

}



int main(int argc, void *argv) {

char **mas = NULL; // двумерный массив строк	
// Одномерный массив кол-ва слов в строках
int N, M;
printf("Введите число строк массива: ");
scanf("%d", &N);
printf("Введите число столбцов массива: ");
scanf("%d", &M);
mtrace();
mas = readMas(N, M);
printMas(mas, N, M);
// freeMas(mas, count);
/*
	A = malloc(N*M*sizeof(int)); ///////////////
	if(!A) {
		printf("Требуемая память не выделена[malloc].\n");
		exit(1);
	}
	for (int i = 0; i < N; i++){
		for (int j = 0; j < M; j++){
			scanf("%s", &A[i][j]);
			printf("A[%d][%d]= ", i, j);
		}
	}
	printf("\n");
	free(A);
*/

/* Input(N, M);
Sort(N, M);
Output(N, M);*/

		return 0;
}